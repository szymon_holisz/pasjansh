#pragma once
#include <windows.h>
#include <iostream>
#include <fstream>
#include <string>
#include <conio.h>
#include <time.h>

using namespace std;

enum SLOT
{
	KOL_1,
	KOL_2,
	KOL_3,
	KOL_4,
	KOL_5,
	KOL_6,
	KOL_7,
	KOL_8,
	STOS
};

enum STATUS
{
    MENU,
    GRA,
    KONIEC
};

struct Punkt
{
	int x;
	int y;
};

class Plansza
{
public:
	Plansza();
	~Plansza();

	void rysujMenu(bool przerysuj);
	void rysujGre(bool przerysuj);
	//H
	void rysujPrzegrana(bool przerysuj);
	void rysujWygrana(bool przerysuj);
	void koniecGry();
	void klawisze(unsigned char znak);
	STATUS status();


private:
	void rysujKarte(SLOT slot);
	void rysujZnak(Punkt p, string text, int kolor);
	//H
	void usunZnak(int ax, int ay);
	void rysujZnak(int ax, int ay, string text, int kolor);
	void zmienStatus(STATUS st);
	string wczytajPlik(string nazwa);
	Punkt *pozycjaKarty(SLOT slot);
	STATUS statusPlanszy;
	short int menuPos;
	short int stosPos;

};

