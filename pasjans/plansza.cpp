#include "stdafx.h"
#include "Plansza.h"
#include "talia.h"


Plansza::Plansza()
{
    menuPos = 0;
    zmienStatus(MENU);
    rysujMenu(true);
}


Plansza::~Plansza()
{
}

string Plansza::wczytajPlik(string nazwa)
{
    ifstream plik;
    string odczyt;
    string s;
    plik.open(nazwa.c_str());

    if(plik.good())
    {
            while(!plik.eof())
            {
                getline(plik, s);
                odczyt += s + "\n";
            }

            plik.close(); // zamkniecie pliku
    }
    else
        cout << "Nie udalo odczytac sie pliku!" << nazwa;

   return odczyt;
}

void Plansza::rysujMenu(bool przerysuj = false)
{
   if(przerysuj)
        cout << wczytajPlik("menu.txt");

   //17,10 18,10 19,10
    rysujZnak(29,17, "NOWA GRA", menuPos == 0 ? 13 : 7);
    rysujZnak(29,18, "AUTORZY", menuPos == 1 ? 13 : 7);
    rysujZnak(29,19, "WYJSCIE", menuPos == 2 ? 13 : 7);


}

//H
void Plansza::rysujPrzegrana(bool przerysuj = false)
{
	if (przerysuj)
		cout << wczytajPlik("przegrana.txt");
}
void Plansza::rysujWygrana(bool przerysuj = false)
{
	if (przerysuj)
		cout << wczytajPlik("wygrana.txt");
}

void Plansza::rysujGre(bool przerysuj = false)
{
    //system("CLS");
    //SetConsoleOutputCP(1251);
    //if(przerysuj)
    //rysujZnak(0,0, "",-1);

	rysujZnak(0, 0, wczytajPlik("gra.txt"), 7);
	// H
	Talia utworzTalie();
	Talia tasujtalie();
}

void Plansza::rysujKarte(SLOT slot)
{
	//H
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 5 ; j++)
		{
			rysujZnak(20+i, 20+j, Talia wyswietltalie(), menuPos == 0 ? 13 : 7);
		}
		
	}

	

	Talia wyswietltalie();
}

void Plansza::zmienStatus(STATUS st)
{
    this->statusPlanszy = st;
}

void Plansza::rysujZnak(Punkt p, string text, int kolor = -1)
{
    COORD coord = { p.x, p.y };
    SetConsoleCursorPosition( GetStdHandle( STD_OUTPUT_HANDLE ), coord );

    if(kolor >= 0)
    {
        SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), kolor);
    }

    cout << text;

    if(kolor >= 0)
        SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 2);
}

void Plansza::rysujZnak(int ax, int ay, string text, int kolor = -1)
{
    Punkt a;
    a.x = ax;
    a.y = ay;
    rysujZnak(a, text, kolor);
}

//H
void Plansza::usunZnak(int ax, int ay)
{
	Punkt a;
	a.x = ax;
	a.y = ay;

}


Punkt *Plansza::pozycjaKarty(SLOT slot)
{
	Punkt *a = new Punkt;


	return a;
}

void Plansza::koniecGry()
{
	//H
	if (Talia ilosckart() == 0 || punkty == 2000)
	{
		cout << "Zdoby�e�: " << punkty << "punkt�w !" << endl;
		rysujWygrana(TRUE);
		zmienStatus(KONIEC);
	} 
	else
	{
		cout << "Zdoby�e�: " << punkty << "punkt�w !" << endl;
		rysujPrzegrana(TRUE);
		zmienStatus(KONIEC);
	}
	
}

STATUS Plansza::status()
{
    return this->statusPlanszy;
}

void Plansza::klawisze(unsigned char znak)
{
    switch(this->status())
    {
        case MENU:
        {

            //gora 38 dol 40 lewo 37 prawo 39 enter 13 escape 76

            if(znak == 72)
            {
               menuPos = menuPos == 0 ? 2 : menuPos-1;
               rysujMenu();
            }
            else if(znak == 80)
            {
               menuPos = menuPos == 2 ? 0 : menuPos+1;
               rysujMenu();
            }
            else if(znak == 13)
            {

                if(menuPos == 0)
                {
                  rysujZnak(0,0,"",-1); system("CLS");  zmienStatus(GRA); rysujGre(true);
                }
                else if(menuPos == 1)
                {
                    //rysujZnak(29,17, "adgfdafgadfg", 6);
                }
                else if(menuPos == 2)
                {

                }
            }


            break;
        }

        case GRA:
        {
			//H
            rysujGre();
			rysujKarte(KOL_1);
			rysujKarte(KOL_2);
			rysujKarte(KOL_3);
			rysujKarte(KOL_4);
			rysujKarte(KOL_5);
			rysujKarte(KOL_6);
			rysujKarte(KOL_7);
			rysujKarte(KOL_8);

			if (znak == 72) //G�ra
			{ 
				menuPos = menuPos == 0 ? 2 : menuPos - 1;
			}
			else if (znak == 80) //D�
			{
				menuPos = menuPos == 2 ? 0 : menuPos + 1;
			}
			else if (znak == 77) //Prawo
			{
				menuPos = menuPos == 2 ? 0 : menuPos + 1;
			}
			else if (znak == 75) //Lewo 
			{
				menuPos = menuPos == 2 ? 0 : menuPos + 1;
			}
			else if (znak == 13)
			{
				//H
				if (karta == kartagracza + 1 || karta == kartagracza - 1)
				{
					usunZnak(0, 0);
					usunPozycje(0);
					punkty = punkty + 50;
				}
			}

            break;
        }

        case KONIEC:
        {


            break;
        }


    }

}
